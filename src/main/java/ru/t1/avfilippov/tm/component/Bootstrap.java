package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.ICommandRepository;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.repository.IUserRepository;
import ru.t1.avfilippov.tm.api.service.*;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.command.project.*;
import ru.t1.avfilippov.tm.command.system.*;
import ru.t1.avfilippov.tm.command.task.*;
import ru.t1.avfilippov.tm.command.user.*;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.avfilippov.tm.exception.system.CommandNotSupportedException;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.repository.CommandRepository;
import ru.t1.avfilippov.tm.repository.ProjectRepository;
import ru.t1.avfilippov.tm.repository.TaskRepository;
import ru.t1.avfilippov.tm.repository.UserRepository;
import ru.t1.avfilippov.tm.service.*;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());

        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationExitCommand());
        registry(new SystemInfoCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }


    public void run(final String[] args) {
        processArguments(args);
        processCommands();
    }

    public void processCommands() {
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHUTTING DOWN***");
            }
        });
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("TEST", "TEST", "test@test.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST1", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("TEST2", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("TEST3", Status.IN_PROGRESS));

        taskService.add(test.getId(), new Task("TEST1", Status.NOT_STARTED));
        taskService.add(admin.getId(), new Task("TEST2", Status.NOT_STARTED));
    }

    public void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String argument = args[0];
        processArgument(argument);
    }


    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
