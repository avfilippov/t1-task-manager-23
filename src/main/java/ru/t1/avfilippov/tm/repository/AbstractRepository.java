package ru.t1.avfilippov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.IRepository;
import ru.t1.avfilippov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        models.removeAll(collection);
    }

}
