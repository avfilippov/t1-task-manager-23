package ru.t1.avfilippov.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login is already exists...");
    }

}
